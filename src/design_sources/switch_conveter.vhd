LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY switch_conveter IS
    Port
    (
    	sw_0 : IN STD_LOGIC;
    	sw_1 : IN STD_LOGIC;
    	sw_2 : IN STD_LOGIC;
    	sw_3 : IN STD_LOGIC;
    	out_0 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END switch_conveter;

ARCHITECTURE BEHAVIORAL OF switch_conveter IS

SIGNAL next_sw_0 : STD_LOGIC;
SIGNAL next_sw_1 : STD_LOGIC;
SIGNAL next_sw_2 : STD_LOGIC;
SIGNAL next_sw_3 : STD_LOGIC;

BEGIN

	out_0(0) <= sw_0;
	out_0(1) <= sw_1;
	out_0(2) <= sw_2;
	out_0(3) <= sw_3;
	
END BEHAVIORAL;
