LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.MATH_REAL.ALL;

ENTITY vga_controller IS
    PORT
    (
        clk100mhz : IN STD_LOGIC;
        btnu : IN STD_LOGIC; -- Up Player 1
        btnl : IN STD_LOGIC; -- Down Player 1
        btnr : IN STD_LOGIC; -- Up Player 2
        btnd : IN STD_LOGIC; -- Down Player 2
        vga_hs : OUT STD_LOGIC;
        vga_vs : OUT STD_LOGIC;
        vga_r : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_g : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_b : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        sw : IN STD_LOGIC_VECTOR(11 DOWNTO 0)
    );
END vga_controller;

ARCHITECTURE BEHAVIORAL OF vga_controller IS

COMPONENT pong
	PORT
	(
		pixel_clock : IN STD_LOGIC;
        btnu : IN STD_LOGIC; -- Up Player 1
        btnl : IN STD_LOGIC; -- Down Player 1
        btnr : IN STD_LOGIC; -- Up Player 2
        btnd : IN STD_LOGIC; -- Down Player 2
		active : IN STD_LOGIC;
		int_h_cntr_reg : IN integer;
		int_v_cntr_reg : IN integer;
		bg_red : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_green : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_blue : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_red_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_green_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_blue_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

COMPONENT switch_conveter
	PORT
	(
		sw_0 : IN STD_LOGIC;
		sw_1 : IN STD_LOGIC;
		sw_2 : IN STD_LOGIC;
		sw_3 : IN STD_LOGIC;
		out_0 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

COMPONENT clk_wiz_0
	PORT
	(
		clk_in1 : IN STD_LOGIC;
		clk_out1 : OUT STD_LOGIC
	);
END COMPONENT;

-------------------------------------------------------------------------

-- VGA 1280x1024@60Hz

-------------------------------------------------------------------------
CONSTANT frame_width : NATURAL := 1280;
CONSTANT frame_height : NATURAL := 1024;

CONSTANT h_front_porch : NATURAL := 48;
CONSTANT h_pulse_width : NATURAL := 112;
CONSTANT h_total_width : NATURAL := 1688;

CONSTANT v_front_porch : NATURAL := 1;
CONSTANT v_pulse_width : NATURAL := 3;
CONSTANT v_total_width : NATURAL := 1066;

CONSTANT h_polarity : STD_LOGIC := '1';
CONSTANT v_polarity : STD_LOGIC := '1';

-------------------------------------------------------------------------

-- VGA Controller specific signals: Counters, Sync, R, G, B

-------------------------------------------------------------------------
-- Pixel clock, IN this case 108 MHz
SIGNAL pixel_clock : STD_LOGIC;
-- The active SIGNAL is used to SIGNAL the active region of the screen (WHEN NOT blank)
SIGNAL active  : STD_LOGIC;

-- Horizontal AND Vertical counters
SIGNAL h_cntr_reg : STD_LOGIC_VECTOR(11 DOWNTO 0) := (OTHERS =>'0');
SIGNAL v_cntr_reg : STD_LOGIC_VECTOR(11 DOWNTO 0) := (OTHERS =>'0');

-- Pipe Horizontal AND Vertical Counters
SIGNAL h_cntr_reg_dly   : STD_LOGIC_VECTOR(11 DOWNTO 0) := (OTHERS => '0');
SIGNAL v_cntr_reg_dly   : STD_LOGIC_VECTOR(11 DOWNTO 0) := (OTHERS => '0');

-- Horizontal AND Vertical Sync
SIGNAL h_sync_reg : STD_LOGIC := NOT(h_polarity);
SIGNAL v_sync_reg : STD_LOGIC := NOT(v_polarity);

-- Pipe Horizontal AND Vertical Sync
SIGNAL h_sync_reg_dly : STD_LOGIC := NOT(h_polarity);
SIGNAL v_sync_reg_dly : STD_LOGIC :=  NOT(v_polarity);

-- VGA R, G AND B signals coming from the main multiplexers
SIGNAL vga_red_cmb   : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL vga_green_cmb : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL vga_blue_cmb  : STD_LOGIC_VECTOR(3 DOWNTO 0);

--The main VGA R, G AND B signals, validated by active
SIGNAL vga_red    : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL vga_green  : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL vga_blue   : STD_LOGIC_VECTOR(3 DOWNTO 0);

-- Register VGA R, G AND B signals
SIGNAL vga_red_reg   : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS =>'0');
SIGNAL vga_green_reg : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS =>'0');
SIGNAL vga_blue_reg  : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS =>'0');

-----------------------------------------------------------

-- Signals for generating the background (moving colorbar)

-----------------------------------------------------------
--SIGNAL cntDyn : integer range 0 to 2**28-1; -- counter for generating the colorbar
SIGNAL int_h_cntr_reg : integer range 0 to h_total_width - 1;
SIGNAL int_v_cntr_reg : integer range 0 to v_total_width - 1;

-- Colorbar red, greeen AND blue signals
SIGNAL bg_red : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL bg_blue : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL bg_green : STD_LOGIC_VECTOR(3 DOWNTO 0);

-- Pipe the colorbar red, green AND blue signals
SIGNAL bg_red_dly : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS => '0');
SIGNAL bg_green_dly : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS => '0');
SIGNAL bg_blue_dly : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS => '0');

BEGIN

    pong_inst : pong
		PORT MAP
		(
			pixel_clock => pixel_clock,
            btnu => btnu,
            btnl => btnl,
            btnr => btnr,
            btnd => btnd,
			active => active,
			int_h_cntr_reg => int_h_cntr_reg,
			int_v_cntr_reg => int_v_cntr_reg,
			bg_red => bg_red,
			bg_green => bg_green,
			bg_blue => bg_blue,
			bg_red_dly => bg_red_dly,
			bg_green_dly => bg_green_dly,
			bg_blue_dly => bg_blue_dly
		);
	
    red_converter : switch_conveter
		PORT MAP
		(
			sw_0 => sw(0),
			sw_1 => sw(1),
			sw_2 => sw(2),
			sw_3 => sw(3),
			out_0 => bg_red
		);
	
    blue_converter : switch_conveter
		PORT MAP
		(
			sw_0 => sw(4),
			sw_1 => sw(5),
			sw_2 => sw(6),
			sw_3 => sw(7),
			out_0 => bg_blue
		);
	
    green_converter : switch_conveter
		PORT MAP
		(
			sw_0 => sw(8),
			sw_1 => sw(9),
			sw_2 => sw(10),
			sw_3 => sw(11),
			out_0 => bg_green
		);
            
    clk_wiz_0_inst : clk_wiz_0
        PORT MAP
        (
        	clk_in1 => clk100mhz,
        	clk_out1 => pixel_clock
        );

	---------------------------------------------------------------
	
	-- Generate Horizontal, Vertical counters AND the Sync signals
	
	---------------------------------------------------------------
	-- Horizontal counter
	PROCESS (pixel_clock)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN
			IF (h_cntr_reg = (h_total_width - 1)) THEN
				h_cntr_reg <= (OTHERS =>'0');
			ELSE
				h_cntr_reg <= h_cntr_reg + 1;
			END IF;
		END IF;
	END PROCESS;

	-- Vertical counter
	PROCESS (pixel_clock)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN
			IF ((h_cntr_reg = (h_total_width - 1)) AND (v_cntr_reg = (v_total_width - 1))) THEN
				v_cntr_reg <= (OTHERS =>'0');
			elsif (h_cntr_reg = (h_total_width - 1)) THEN
				v_cntr_reg <= v_cntr_reg + 1;
			END IF;
		END IF;
	END PROCESS;

	-- Horizontal sync
	PROCESS (pixel_clock)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN
			IF (h_cntr_reg >= (h_front_porch + frame_width - 1)) AND (h_cntr_reg < (h_front_porch + frame_width + h_pulse_width - 1)) THEN
				h_sync_reg <= h_polarity;
			ELSE
				h_sync_reg <= NOT(h_polarity);
			END IF;
		END IF;
	END PROCESS;
    
	-- Vertical sync
	PROCESS (pixel_clock)
		BEGIN
		IF (rising_edge(pixel_clock)) THEN
			IF (v_cntr_reg >= (v_front_porch + frame_height - 1)) AND (v_cntr_reg < (v_front_porch + frame_height + v_pulse_width - 1)) THEN
				v_sync_reg <= v_polarity;
			ELSE
				v_sync_reg <= NOT(v_polarity);
			END IF;
		END IF;
	END PROCESS;
	
	-----------------

	-- Active Signal

	-----------------
	active <= '1' WHEN h_cntr_reg_dly < frame_width AND v_cntr_reg_dly < frame_height
	ELSE '0';

	---------------------------------------
	
	-- Generate moving colorbar background
	
	---------------------------------------
	--PROCESS(pixel_clock)
	--BEGIN
	--	IF(rising_edge(pixel_clock)) THEN
	--		cntdyn <= cntdyn + 1;
	--	END IF;
	--END PROCESS;

	int_h_cntr_reg <= conv_integer(h_cntr_reg);
	int_v_cntr_reg <= conv_integer(v_cntr_reg);
	
	--bg_red <= conv_std_logic_vector((-int_v_cntr_reg - int_h_cntr_reg - cntDyn/2**20),8)(7 DOWNTO 4);
	--bg_green <= conv_std_logic_vector((int_h_cntr_reg - cntDyn/2**20),8)(7 DOWNTO 4);
	--bg_blue <= conv_std_logic_vector((int_v_cntr_reg - cntDyn/2**20),8)(7 DOWNTO 4);
	--bg_red <= bg_red;
	--bg_green <= bg_green;
	--bg_blue <= bg_blue;

    ---------------------------------------------------------------------------------------------------
    
    -- Register Outputs coming from the displaying components AND the horizontal AND vertical counters
    
    ---------------------------------------------------------------------------------------------------
	PROCESS (pixel_clock)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN
			h_cntr_reg_dly <= h_cntr_reg;
			v_cntr_reg_dly <= v_cntr_reg;
		END IF;
	END PROCESS;

    ----------------------------------
    
    -- VGA Output Muxing
    
    ----------------------------------
    vga_red <= bg_red_dly;
    vga_green <= bg_green_dly;
    vga_blue <= bg_blue_dly;
    
    ------------------------------------------------------------
    -- Turn Off VGA RBG Signals IF outside of the active screen
    -- Make a 4-bit AND logic with the R, G AND B signals
    ------------------------------------------------------------
    vga_red_cmb <= (active & active & active & active) AND vga_red;
    vga_green_cmb <= (active & active & active & active) AND vga_green;
    vga_blue_cmb <= (active & active & active & active) AND vga_blue;
    
    -- Register Outputs
    PROCESS (pixel_clock)
    BEGIN
        IF (rising_edge(pixel_clock)) THEN
            v_sync_reg_dly <= v_sync_reg;
            h_sync_reg_dly <= h_sync_reg;
            vga_red_reg <= vga_red_cmb;
            vga_green_reg <= vga_green_cmb;
            vga_blue_reg <= vga_blue_cmb;      
        END IF;
    END PROCESS;

    -- Assign outputs
    vga_hs <= h_sync_reg_dly;
    vga_vs <= v_sync_reg_dly;
    vga_r <= vga_red_reg;
    vga_g <= vga_green_reg;
    vga_b <= vga_blue_reg;

END Behavioral;
