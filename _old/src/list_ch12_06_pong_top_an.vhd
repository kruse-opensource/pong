-- Listing 12.6
library ieee;
use ieee.std_logic_1164.all;
entity pong_top_an is
   port (
      clk,reset: in std_logic;
      btn: in std_logic_vector (1 downto 0);
      hsync, vsync: out  std_logic;
      --rgb: out std_logic_vector(2 downto 0);
      VGA_R: out std_logic_vector(3 downto 0);
      VGA_G: out std_logic_vector(3 downto 0);
      VGA_B: out std_logic_vector(3 downto 0)
   );
end pong_top_an;

architecture arch of pong_top_an is
   signal pixel_x, pixel_y: std_logic_vector (9 downto 0);
   signal video_on, pixel_tick: std_logic;
   --signal rgb_reg, rgb_next: std_logic_vector(2 downto 0);
   signal VGA_R_reg, VGA_R_next: std_logic_vector(3 downto 0);
   signal VGA_G_reg, VGA_G_next: std_logic_vector(3 downto 0);
   signal VGA_B_reg, VGA_B_next: std_logic_vector(3 downto 0);
begin
   -- instantiate VGA sync
   vga_sync_unit: entity work.vga_sync
      port map
      (
	     clk=>clk,
      	 reset=>reset,
	  	 video_on=>video_on,
	  	 p_tick=>pixel_tick,
	   	 hsync=>hsync,
	   	 vsync=>vsync,
         pixel_x=>pixel_x,
         pixel_y=>pixel_y
	   );
   -- instantiate graphic generator
   pong_graph_an_unit: entity work.pong_graph_animate
      port map
      (
      	 clk=>clk,
      	 reset=>reset,
		 btn=>btn,
		 video_on=>video_on,
		 pixel_x=>pixel_x,
		 pixel_y=>pixel_y,
		 --graph_rgb => rgb_next,
		 graph_VGA_R => VGA_R_next,
		 graph_VGA_G => VGA_G_next,
		 graph_VGA_B => VGA_B_next
	  );
   -- rgb buffer
   process (clk)
   begin
      if (clk'event and clk='1') then
         if (pixel_tick='1') then
         	--rgb_reg <= rgb_next;
         	VGA_R_reg <= VGA_R_next;
         	VGA_G_reg <= VGA_G_next;
         	VGA_B_reg <= VGA_B_next;
         end if;
      end if;
   end process;
   --rgb <= rgb_reg;
   VGA_R <= VGA_R_reg;
   VGA_G <= VGA_G_reg;
   VGA_B <= VGA_B_reg;
end arch;