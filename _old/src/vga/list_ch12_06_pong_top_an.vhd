-- Listing 12.6
library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity pong_top_an is
   	port
	(
		clk,reset: in std_logic;
      	btn: in std_logic_vector (1 downto 0);
      	hsync, vsync: out  std_logic;
      	--rgb: out std_logic_vector(2 downto 0);
         VGA_R: out std_logic_vector(3 downto 0);
         VGA_G: out std_logic_vector(3 downto 0);
         VGA_B: out std_logic_vector(3 downto 0)
	);
end pong_top_an;

architecture arch of pong_top_an is

	signal pixel_x, pixel_y: std_logic_vector (11 downto 0);
	signal video_on, pixel_tick: std_logic;
	--signal rgb_reg, rgb_next: std_logic_vector(2 downto 0);
   	signal VGA_R_reg, VGA_R_next: std_logic_vector(3 downto 0);
   	signal VGA_G_reg, VGA_G_next: std_logic_vector(3 downto 0);
   	signal VGA_B_reg, VGA_B_next: std_logic_vector(3 downto 0);
	signal clk_out1 : std_logic;
	signal CLKFBOUT : std_logic;
   
begin

   -- instantiate VGA sync
	vga_sync_unit: entity work.vga_sync
		port map
		(
			clk => clk_out1,
			pixel_clock => clk_out1,
			reset => reset,
         	video_on => video_on,
         	p_tick => pixel_tick,
         	hsync => hsync,
         	vsync => vsync,
         	pixel_x => pixel_x,
         	pixel_y => pixel_y
		);
               
   -- instantiate graphic generator
--	pong_graph_an_unit: entity work.pong_graph_animate
--		port map
--      	(
--         	clk => clk_out1,
--        	reset => reset,
--			btn => btn,
--			video_on => video_on,
--			pixel_x => pixel_x,
--			pixel_y => pixel_y,
--			--graph_rgb => rgb_next,
--         	graph_VGA_R => VGA_R_next,
--         	graph_VGA_G => VGA_G_next,
--         	graph_VGA_B => VGA_B_next
--		);
                
   -- rgb buffer
	process (clk_out1)
   	begin
		if rising_edge(clk_out1) then
	 		--if (pixel_tick = '1') then
				--rgb_reg <= rgb_next;
         		VGA_R_reg <= VGA_R_next;
         		VGA_G_reg <= VGA_G_next;
         		VGA_B_reg <= VGA_B_next;
	 		--end if;
		end if;
   	end process;
   
   	--rgb <= rgb_reg;
      --VGA_R <= VGA_R_reg;
      --VGA_G <= VGA_G_reg;
      --VGA_B <= VGA_B_reg;
      VGA_R <= "1111";
      VGA_G <= "1111";
      VGA_B <= "1111";
   
   -- PLLE2_BASE: Base Phase Locked Loop (PLL)
   --             Artix-7
   -- Xilinx HDL Language Template, version 2018.3

   PLLE2_BASE_inst : PLLE2_BASE
   generic map (
      BANDWIDTH => "OPTIMIZED",  -- OPTIMIZED, HIGH, LOW
      CLKFBOUT_MULT => 54,        -- Multiply value for all CLKOUT, (2-64)
      CLKFBOUT_PHASE => 0.0,     -- Phase offset in degrees of CLKFB, (-360.000-360.000).
      CLKIN1_PERIOD => 100.0,      -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
      CLKOUT0_DIVIDE => 50,
      CLKOUT1_DIVIDE => 1,
      CLKOUT2_DIVIDE => 1,
      CLKOUT3_DIVIDE => 1,
      CLKOUT4_DIVIDE => 1,
      CLKOUT5_DIVIDE => 1,
      -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
      CLKOUT0_DUTY_CYCLE => 0.5,
      CLKOUT1_DUTY_CYCLE => 0.5,
      CLKOUT2_DUTY_CYCLE => 0.5,
      CLKOUT3_DUTY_CYCLE => 0.5,
      CLKOUT4_DUTY_CYCLE => 0.5,
      CLKOUT5_DUTY_CYCLE => 0.5,
      -- CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
      CLKOUT0_PHASE => 0.0,
      CLKOUT1_PHASE => 0.0,
      CLKOUT2_PHASE => 0.0,
      CLKOUT3_PHASE => 0.0,
      CLKOUT4_PHASE => 0.0,
      CLKOUT5_PHASE => 0.0,
      DIVCLK_DIVIDE => 1,        -- Master division value, (1-56)
      REF_JITTER1 => 0.0,        -- Reference input jitter in UI, (0.000-0.999).
      STARTUP_WAIT => "FALSE"    -- Delay DONE until PLL Locks, ("TRUE"/"FALSE")
   )
   port map (
      -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
      CLKOUT0 => clk_out1,   -- 1-bit output: CLKOUT0
      CLKOUT1 => open,   -- 1-bit output: CLKOUT1
      CLKOUT2 => open,   -- 1-bit output: CLKOUT2
      CLKOUT3 => open,   -- 1-bit output: CLKOUT3
      CLKOUT4 => open,   -- 1-bit output: CLKOUT4
      CLKOUT5 => open,   -- 1-bit output: CLKOUT5
      -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
      CLKFBOUT => CLKFBOUT, -- 1-bit output: Feedback clock
      LOCKED => open,     -- 1-bit output: LOCK
      CLKIN1 => clk,     -- 1-bit input: Input clock
      -- Control Ports: 1-bit (each) input: PLL control ports
      PWRDWN => '0',     -- 1-bit input: Power-down
      RST => '0',           -- 1-bit input: Reset
      -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
      CLKFBIN => CLKFBOUT    -- 1-bit input: Feedback clock
   );

   -- End of PLLE2_BASE_inst instantiation

   
end arch;