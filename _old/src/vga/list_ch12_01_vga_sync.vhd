-- Listing 12.1
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vga_sync is
	port
	(
		clk : in std_logic;
      	pixel_clock : in std_logic;
      	reset: in std_logic;
      	hsync : out std_logic;
      	vsync : out std_logic;
      	video_on : out std_logic;
      	p_tick : out std_logic;
      	pixel_x : out std_logic_vector (11 downto 0);
      	pixel_y : out std_logic_vector (11 downto 0)
	);
end vga_sync;

architecture arch of vga_sync is

   -- VGA 640x480 60 Hz
   -- pixel_clock = 25.175 MHz
   --constant HD: integer:=640; --horizontal display area
   --constant HF: integer:=16 ; --h. front porch
   --constant HR: integer:=96 ; --h. retrace
   --constant HB: integer:=48 ; --h. back porch
   --constant VD: integer:=480; --vertical display area
   --constant VF: integer:=10;  --v. front porch
   --constant VR: integer:=2;   --v. retrace
   --constant VB: integer:=33;  --v. back porch
   -- h_sync Polarity n
   -- v_sync Polarity n

   -- VGA 1280x1024 60 Hz
   -- pixel_clock = 108 MHz
   constant HD: integer:=1280; --horizontal display area
   constant HF: integer:=48 ; --h. front porch
   constant HR: integer:=112 ; --h. retrace
   constant HB: integer:=248 ; --h. back porch
   constant VD: integer:=1024; --vertical display area
   constant VF: integer:=1;  --v. front porch
   constant VR: integer:=3;   --v. retrace
   constant VB: integer:=38;  --v. back porch
   -- h_sync Polarity = p
   -- v_sync Polarity = p
   
   -- mod-2 counter
   signal mod2_reg, mod2_next: std_logic;
   -- sync counters
   signal v_count_reg, v_count_next: unsigned(11 downto 0);
   signal h_count_reg, h_count_next: unsigned(11 downto 0);
   -- output buffer
   signal v_sync_reg, h_sync_reg: std_logic;
   signal v_sync_next, h_sync_next: std_logic;
   -- status signal
   signal h_end, v_end, pixel_tick: std_logic;
   
begin

   -- registers
   process (clk,reset)
   begin
      if reset='1' then
         mod2_reg <= '0';
         v_count_reg <= (others=>'0');
         h_count_reg <= (others=>'0');
         v_sync_reg <= '0';
         h_sync_reg <= '0';
      elsif (clk'event and clk='1') then
         mod2_reg <= mod2_next;
         v_count_reg <= v_count_next;
         h_count_reg <= h_count_next;
         v_sync_reg <= v_sync_next;
         h_sync_reg <= h_sync_next;
      end if;
   end process;
   
   --mod2_next <= not mod2_reg;
   
   --pixel_tick <= '1' when mod2_reg='1' else '0';
   
	process (pixel_clock)
	begin
		if rising_edge(pixel_clock) then
			pixel_tick <= '1';
		end if;
		if falling_edge(pixel_clock) then
			pixel_tick <= '0';
		end if;
	end process;
   
   h_end <= '1' when h_count_reg=(HD+HF+HB+HR-1) else '0';
   v_end <= '1' when v_count_reg=(VD+VF+VB+VR-1) else '0';
   
   process (h_count_reg,h_end,pixel_tick)
   begin
      if pixel_tick='1' then
         if h_end='1' then
            h_count_next <= (others=>'0');
         else
            h_count_next <= h_count_reg + 1;
         end if;
      else
         h_count_next <= h_count_reg;
      end if;
   end process;
   
   process (v_count_reg,h_end,v_end,pixel_tick)
   begin
      if pixel_tick='1' and h_end='1' then
         if (v_end='1') then
            v_count_next <= (others=>'0');
         else
            v_count_next <= v_count_reg + 1;
         end if;
      else
         v_count_next <= v_count_reg;
      end if;
   end process;
   
   h_sync_next <= '1' when (h_count_reg>=(HD+HF)) and (h_count_reg<=(HD+HF+HR-1)) else '0';
   v_sync_next <= '1' when (v_count_reg>=(VD+VF)) and (v_count_reg<=(VD+VF+VR-1)) else '0';
      
   video_on <= '1' when (h_count_reg<HD) and (v_count_reg<VD) else '0';
      
   hsync <= not h_sync_reg;
   vsync <= v_sync_reg;
   pixel_x <= std_logic_vector(h_count_reg);
   pixel_y <= std_logic_vector(v_count_reg);
   p_tick <= pixel_tick;
   
end arch;
