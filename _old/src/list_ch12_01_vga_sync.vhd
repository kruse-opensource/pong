-- Listing 12.1
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity vga_sync is
   port(
      clk, reset: in std_logic;
      hsync, vsync: out std_logic;
      video_on, p_tick: out std_logic;
      pixel_x, pixel_y: out std_logic_vector (9 downto 0)
    );
end vga_sync;

architecture arch of vga_sync is
   -- VGA 640-by-480 sync parameters
   --constant HD: integer := 640; --horizontal display area
   --constant HF: integer := 16 ; --h. front porch
   --constant HR: integer := 96 ; --h. sync pulse
   --constant HB: integer := 48 ; --h. back porch
   --constant VD: integer := 480; --vertical display area
   --constant VF: integer := 10;  --v. front porch
   --constant VR: integer := 2;   --v. sync pulse
   --constant VB: integer := 33;  --v. back porch

   -- VGA 800-by-600 sync parameters
   constant pixel_clock: integer := 49.5;
   constant HD: integer := 800; --horizontal display area
   constant HF: integer := 16 ; --h. front porch
   constant HR: integer := 80 ; --h. sync pulse
   constant HB: integer := 160 ; --h. back porch
   constant VD: integer := 600; --vertical display area
   constant VF: integer := 1;  --v. front porch
   constant VR: integer := 3;   --v. sync pulse
   constant VB: integer := 21;  --v. back porch
   constant h_sync_polarity: integer := 1; -- 0 = negative, 1 = positive
   constant v_sync_polarity: integer := 1; -- 0 = negative, 1 = positive


   -- counter
   signal tick_clock: std_logic;
   signal tick_counter: std_logic_vector(9 downto 0);
   -- sync counters
   signal v_count_reg, v_count_next: unsigned(9 downto 0);
   signal h_count_reg, h_count_next: unsigned(9 downto 0);
   -- output buffer
   signal v_sync_reg, h_sync_reg: std_logic;
   signal v_sync_next, h_sync_next: std_logic;
   -- status signal
   signal h_end, v_end, pixel_tick: std_logic;

begin
   -- registers
   process (clk,reset)
   begin
      if reset='1' then
         tick_clock <= '0';
         v_count_reg <= (others=>'0');
         h_count_reg <= (others=>'0');
         v_sync_reg <= '0';
         h_sync_reg <= '0';
      elsif (clk'event and clk='1') then
         -- Enable Tick
         tick_counter := tick_counter + 1;
         if (tick_counter = ((100000000)/(pixel_clock * 1000000))) then
            tick_clock <= '1';
         else
            tick_clock <= '0';
         end if;

         v_count_reg <= v_count_next;
         h_count_reg <= h_count_next;
         v_sync_reg <= v_sync_next;
         h_sync_reg <= h_sync_next;
      end if;
   end process;

   -- pixel tick
   pixel_tick <= '1' when tick_clock='1' else '0';

   -- status
   h_end <=  -- end of horizontal counter
      '1' when h_count_reg=(HD+HF+HB+HR-1) else --799
      '0';

   v_end <=  -- end of vertical counter
      '1' when v_count_reg=(VD+VF+VB+VR-1) else --524
      '0';

   -- Gorizontal sync counter
   process (h_count_reg,h_end,pixel_tick)
   begin
      if pixel_tick='1' then
         if h_end='1' then
            h_count_next <= (others=>'0');
         else
            h_count_next <= h_count_reg + 1;
         end if;
      else
         h_count_next <= h_count_reg;
      end if;
   end process;

   -- Vertical sync counter
   process (v_count_reg,h_end,v_end,pixel_tick)
   begin
      if pixel_tick='1' and h_end='1' then
         if (v_end='1') then
            v_count_next <= (others=>'0');
         else
            v_count_next <= v_count_reg + 1;
         end if;
      else
         v_count_next <= v_count_reg;
      end if;
   end process;

   -- horizontal and vertical sync, buffered to avoid glitch
   h_sync_next <=
      '1' when (h_count_reg>=(HD+HF))
           and (h_count_reg<=(HD+HF+HR-1)) else
      '0';
   v_sync_next <=
      '1' when (v_count_reg>=(VD+VF))
           and (v_count_reg<=(VD+VF+VR-1)) else
      '0';

   -- video on/off
   video_on <=
      '1' when (h_count_reg<HD) and (v_count_reg<VD) else
      '0';

   -- output signal
   if (h_sync_polarity = 0) then
      hsync <= h_sync_reg;
   else
      hsync <= not h_sync_reg;
   end if;

   if (v_sync_polarity = 0) then
      vsync <= v_sync_reg;
   else
      vsync <= not v_sync_reg;
   end if;

   pixel_x <= std_logic_vector(h_count_reg);
   pixel_y <= std_logic_vector(v_count_reg);
   p_tick <= pixel_tick;
end arch;
