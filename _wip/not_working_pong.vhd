LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY pong IS
    Port
    (
        pixel_clock : IN STD_LOGIC;
        active : IN STD_LOGIC;
        int_h_cntr_reg : IN integer;
        int_v_cntr_reg : IN integer;
    	bg_red : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_green : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_blue : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_red_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_green_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_blue_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END pong;

ARCHITECTURE BEHAVIORAL OF pong IS

SIGNAL int_player1_height : INTEGER := 64;
SIGNAL int_player1_width : INTEGER := 8;
SIGNAL int_player1_start_h : INTEGER := 44;
SIGNAL int_player1_start_v : INTEGER := 512;
SIGNAL int_player1_position_h : INTEGER := int_player1_start_h;
SIGNAL int_player1_position_v : INTEGER := int_player1_start_v;
SIGNAL int_player1_move : INTEGER RANGE 0 to 10800000 := 0;
SIGNAL int_player1_move_up : STD_LOGIC := '0';
SIGNAL int_player1_move_down : STD_LOGIC := '0';
SIGNAL next_int_player1_move_up : STD_LOGIC := '0';
SIGNAL next_int_player1_move_down : STD_LOGIC := '0';

SIGNAL int_player2_height : INTEGER := 64;
SIGNAL int_player2_width : INTEGER := 8;
SIGNAL int_player2_start_h : INTEGER := 1236;
SIGNAL int_player2_start_v : INTEGER := 512;
SIGNAL int_player2_position_h : INTEGER := int_player2_start_h;
SIGNAL int_player2_position_v : INTEGER := int_player2_start_v;
SIGNAL int_player2_move : INTEGER RANGE 0 to 10800000 := 0;
SIGNAL int_player2_move_up : STD_LOGIC := '0';
SIGNAL int_player2_move_down : STD_LOGIC := '0';
SIGNAL next_int_player2_move_up : STD_LOGIC := '0';
SIGNAL next_int_player2_move_down : STD_LOGIC := '0';

SIGNAL int_ball_height : INTEGER := 8;
SIGNAL int_ball_width : INTEGER := 8;
SIGNAL int_ball_start_h : INTEGER := 645;
SIGNAL int_ball_start_v : INTEGER := 512;
SIGNAL int_ball_position_h : INTEGER := int_ball_start_h;
SIGNAL int_ball_position_v : INTEGER := int_ball_start_v;

BEGIN

	PROCESS (int_player1_move)
	BEGIN
		next_int_player1_move_up <= int_player1_move_up;
		next_int_player1_move_down <= int_player1_move_down;

		IF int_player1_move = 10800000 THEN
			next_int_player1_move_down <= '1';
		END IF;
	END PROCESS;

	PROCESS (pixel_clock,int_player1_move_up,int_player1_move_down)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN

			int_player1_move_up <= next_int_player1_move_up;
			int_player1_move_down <= next_int_player1_move_down;

			int_player1_move <= int_player2_move + 1;

			-- Move Player 1 Horizontally
			IF int_player1_move_up = '1' THEN
				int_player1_position_v <= int_player1_position_v - 1;
				next_int_player1_move_up <= '0';
			END IF;
			IF int_player1_move_down = '1' THEN
				int_player1_position_v <= int_player1_position_v + 1;
				next_int_player1_move_down <= '0';
			END IF;

			-- Render Player 1
			IF
			(
				active = '1' AND
				(int_h_cntr_reg >= int_player1_position_h) AND (int_h_cntr_reg <= int_player1_position_h + int_player1_width) AND
				(int_v_cntr_reg >= int_player1_position_v - int_player1_height / 2) AND (int_v_cntr_reg <= int_player1_position_v + int_player1_height / 2)
			) THEN
				bg_red_dly <= "0011";
				bg_green_dly <= "0011";
				bg_blue_dly <= "0011";

			-- Render Player 2
			ELSIF
			(
				active = '1' AND
				(int_h_cntr_reg >= int_player2_position_h - int_player2_width) AND (int_h_cntr_reg <= int_player2_position_h) AND
				(int_v_cntr_reg >= int_player2_position_v - int_player2_height / 2) AND (int_v_cntr_reg <= int_player2_position_v + int_player2_height / 2)
			) THEN
				bg_red_dly <= "0011";
				bg_green_dly <= "0011";
				bg_blue_dly <= "0011";

			-- Render Ball
			ELSIF
			(
				active = '1' AND
				(int_h_cntr_reg >= int_ball_position_h - int_ball_width) AND (int_h_cntr_reg <= int_ball_position_h) AND
				(int_v_cntr_reg >= int_ball_position_v - int_ball_height / 2) AND (int_v_cntr_reg <= int_ball_position_v + int_ball_height / 2)
			) THEN
				bg_red_dly <= "0011";
				bg_green_dly <= "0011";
				bg_blue_dly <= "0011";

			-- Render Nothing in-between
			ELSE
				bg_red_dly <= "0000";
				bg_green_dly <= "0000";
				bg_blue_dly <= "0000";
			END IF;
		END IF;
	END PROCESS;
	
END BEHAVIORAL;
