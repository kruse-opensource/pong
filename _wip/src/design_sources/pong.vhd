LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY pong IS
    Port
    (
        pixel_clock : IN STD_LOGIC;
        active : IN STD_LOGIC;
        int_h_cntr_reg : IN integer;
        int_v_cntr_reg : IN integer;
    	bg_red : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_green : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_blue : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_red_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_green_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    	bg_blue_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END pong;

ARCHITECTURE BEHAVIORAL OF pong IS

BEGIN

    IF (rising_edge(pixel_clock)) THEN
        IF
		(
			active = '1' AND
			int_h_cntr_reg >= 500 AND int_h_cntr_reg <= 510 AND
			int_v_cntr_reg >= 500 AND int_v_cntr_reg <= 600
		) THEN
            bg_red_dly <= bg_red;
            bg_green_dly <= bg_green;
            bg_blue_dly <= bg_blue;
        ELSE
            bg_red_dly <= "0000";
            bg_green_dly <= "0000";
            bg_blue_dly <= "0000";
        END IF;
	END IF;
	END PROCESS;
	
END BEHAVIORAL;
