LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.MATH_REAL.ALL;

ENTITY top IS
    PORT
    (
        clk100mhz : IN STD_LOGIC;
        btnu : IN STD_LOGIC; -- Up Player 1
        btnl : IN STD_LOGIC; -- Down Player 1
        btnr : IN STD_LOGIC; -- Up Player 2
        btnd : IN STD_LOGIC; -- Down Player 2
        vga_hs : OUT STD_LOGIC;
        vga_vs : OUT STD_LOGIC;
        vga_r : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_g : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_b : OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
    );
END top;

ARCHITECTURE behavioral OF top IS

--------------------------------------------

-- Declare Components

--------------------------------------------
-- 108 MHz PLL for pixel_clock.
COMPONENT clk_wiz_0
	PORT
	(
		clk_in1 : IN STD_LOGIC;
		clk_out1 : OUT STD_LOGIC
	);
END COMPONENT;

-- VGA Controller
COMPONENT vga_controller IS
    PORT
    (
        pixel_clock : IN STD_LOGIC;
        vga_r_active : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_g_active : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_b_active : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
        int_h_counter_reg_out : OUT INTEGER;
        int_v_counter_reg_out : OUT INTEGER;
        active_out : OUT STD_LOGIC;
        vga_hs : OUT STD_LOGIC;
        vga_vs : OUT STD_LOGIC;
        vga_r : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_g : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_b : OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
    );
END COMPONENT;

-- Abstracted game logic.
COMPONENT pong
	PORT
	(
		pixel_clock : IN STD_LOGIC;
        btnu : IN STD_LOGIC; -- Up Player 1
        btnl : IN STD_LOGIC; -- Down Player 1
        btnr : IN STD_LOGIC; -- Up Player 2
        btnd : IN STD_LOGIC; -- Down Player 2
		active : IN STD_LOGIC;
		int_h_counter_reg : IN INTEGER;
		int_v_counter_reg : IN INTEGER;
		vga_r_active : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		vga_g_active : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		vga_b_active : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

--------------------------------------------

-- Declare Components

--------------------------------------------

-- Pixel clock Signal.
SIGNAL pixel_clock : STD_LOGIC;

-- VGA signals that go through active check.
SIGNAL sig_vga_r_active    : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL sig_vga_g_active  : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL sig_vga_b_active   : STD_LOGIC_VECTOR(3 DOWNTO 0);

SIGNAL sig_int_h_counter_reg_out : INTEGER;
SIGNAL sig_int_v_counter_reg_out : INTEGER;
SIGNAL sig_active_out : STD_LOGIC;

BEGIN

	--------------------------------------------

	-- Instantiate Components

	--------------------------------------------
	clk_wiz_0_inst : clk_wiz_0
        PORT MAP
        (
        	clk_in1 => clk100mhz,
        	clk_out1 => pixel_clock
        );
        
    vga_controller_inst : vga_controller
        PORT MAP
        (
            pixel_clock => pixel_clock,
            vga_r_active => sig_vga_r_active,
			vga_g_active => sig_vga_g_active,
			vga_b_active => sig_vga_b_active,
			int_h_counter_reg_out => sig_int_h_counter_reg_out,
			int_v_counter_reg_out => sig_int_v_counter_reg_out,
            active_out => sig_active_out,
            vga_hs => vga_hs,
            vga_vs => vga_vs,
            vga_r => vga_r,
            vga_g => vga_g,
            vga_b => vga_b
        );

    pong_inst : pong
		PORT MAP
		(
			pixel_clock => pixel_clock,
            btnu => btnu,
            btnl => btnl,
            btnr => btnr,
            btnd => btnd,
			active => sig_active_out,
			int_h_counter_reg => sig_int_h_counter_reg_out,
			int_v_counter_reg => sig_int_v_counter_reg_out,
			vga_r_active => sig_vga_r_active,
			vga_g_active => sig_vga_g_active,
			vga_b_active => sig_vga_b_active
		);
	--------------------------------------------

	-- Instantiate Components

	--------------------------------------------

END behavioral;
