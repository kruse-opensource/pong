LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.MATH_REAL.ALL;

ENTITY vga_controller IS
    PORT
    (
        pixel_clock : IN STD_LOGIC;
        vga_r_active : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_g_active : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_b_active : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
        int_h_counter_reg_out : OUT INTEGER;
        int_v_counter_reg_out : OUT INTEGER;
        active_out : OUT STD_LOGIC;
        vga_hs : OUT STD_LOGIC;
        vga_vs : OUT STD_LOGIC;
        vga_r : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_g : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_b : OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
    );
END vga_controller;

ARCHITECTURE behavioral OF vga_controller IS

--------------------------------------------

-- VGA Config

--------------------------------------------
-- 1280x1024@60Hz
CONSTANT frame_width : INTEGER := 1280;
CONSTANT frame_height : INTEGER := 1024;

CONSTANT h_front_porch : INTEGER := 48;
CONSTANT h_sync_pulse : INTEGER := 112;
CONSTANT h_total_width : INTEGER := 1688;

CONSTANT v_front_porch : INTEGER := 1;
CONSTANT v_sync_pulse : INTEGER := 3;
CONSTANT v_total_width : INTEGER := 1066;

CONSTANT h_polarity : STD_LOGIC := '1';
CONSTANT v_polarity : STD_LOGIC := '1';
--------------------------------------------

-- VGA Config

--------------------------------------------

-- Pixel clock Signal.
--SIGNAL pixel_clock : STD_LOGIC;

-- Active signal, is 1 when screen is ready to draw.
SIGNAL active  : STD_LOGIC;

-- Horizontal and vertical counter registers.
SIGNAL h_counter_reg : STD_LOGIC_VECTOR(11 DOWNTO 0) := (OTHERS =>'0');
SIGNAL v_counter_reg : STD_LOGIC_VECTOR(11 DOWNTO 0) := (OTHERS =>'0');

-- Delay horizontal and vertical counter registers.
SIGNAL h_counter_reg_delayed   : STD_LOGIC_VECTOR(11 DOWNTO 0) := (OTHERS => '0');
SIGNAL v_counter_reg_delayed   : STD_LOGIC_VECTOR(11 DOWNTO 0) := (OTHERS => '0');

-- Horizontal and vertical sync registers.
SIGNAL vga_hs_reg : STD_LOGIC := NOT(h_polarity);
SIGNAL vga_vs_reg : STD_LOGIC := NOT(v_polarity);

-- Delay horizontal and vertical sync registers.
SIGNAL vga_hs_reg_delayed : STD_LOGIC := NOT(h_polarity);
SIGNAL vga_vs_reg_delayed : STD_LOGIC :=  NOT(v_polarity);

-- VGA signals coming from multiplexer.
SIGNAL vga_r_mux   : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL vga_g_mux : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL vga_b_mux  : STD_LOGIC_VECTOR(3 DOWNTO 0);

-- VGA signals coming from output multiplexer.
SIGNAL vga_r_out_mux   : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL vga_g_out_mux : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL vga_b_out_mux  : STD_LOGIC_VECTOR(3 DOWNTO 0);

-- VGA signals that go through active check.
--SIGNAL vga_r_active    : STD_LOGIC_VECTOR(3 DOWNTO 0);
--SIGNAL vga_g_active  : STD_LOGIC_VECTOR(3 DOWNTO 0);
--SIGNAL vga_b_active   : STD_LOGIC_VECTOR(3 DOWNTO 0);

-- VGA registers.
SIGNAL vga_r_reg   : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS =>'0');
SIGNAL vga_g_reg : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS =>'0');
SIGNAL vga_b_reg  : STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS =>'0');

-- Signals for converting h and v counter registers to int.
SIGNAL int_h_counter_reg : INTEGER RANGE 0 TO h_total_width - 1;
SIGNAL int_v_counter_reg : INTEGER RANGE 0 TO v_total_width - 1;

BEGIN

	-- Horizontal counter.
	PROCESS (pixel_clock)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN
			IF (h_counter_reg = (h_total_width - 1)) THEN
				h_counter_reg <= (OTHERS =>'0');
			ELSE
				h_counter_reg <= h_counter_reg + 1;
			END IF;
		END IF;
	END PROCESS;

	-- Vertical counter.
	PROCESS (pixel_clock)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN
			IF ((h_counter_reg = (h_total_width - 1)) AND (v_counter_reg = (v_total_width - 1))) THEN
				v_counter_reg <= (OTHERS =>'0');
			ELSIF (h_counter_reg = (h_total_width - 1)) THEN
				v_counter_reg <= v_counter_reg + 1;
			END IF;
		END IF;
	END PROCESS;

	-- Horizontal sync.
	PROCESS (pixel_clock)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN
			IF (h_counter_reg >= (h_front_porch + frame_width - 1)) AND (h_counter_reg < (h_front_porch + frame_width + h_sync_pulse - 1)) THEN
				vga_hs_reg <= h_polarity;
			ELSE
				vga_hs_reg <= NOT(h_polarity);
			END IF;
		END IF;
	END PROCESS;
    
	-- Vertical sync.
	PROCESS (pixel_clock)
		BEGIN
		IF (rising_edge(pixel_clock)) THEN
			IF (v_counter_reg >= (v_front_porch + frame_height - 1)) AND (v_counter_reg < (v_front_porch + frame_height + v_sync_pulse - 1)) THEN
				vga_vs_reg <= v_polarity;
			ELSE
				vga_vs_reg <= NOT(v_polarity);
			END IF;
		END IF;
	END PROCESS;
	
	-- Delay current h and v counters to use in active signal.
	PROCESS (pixel_clock)
	BEGIN
		IF (rising_edge(pixel_clock)) THEN
			h_counter_reg_delayed <= h_counter_reg;
			v_counter_reg_delayed <= v_counter_reg;
		END IF;
	END PROCESS;

	-- Check if delayed h and v counters are within the specified frame.
	active <= '1' WHEN h_counter_reg_delayed < frame_width AND v_counter_reg_delayed < frame_height
	ELSE '0';
	
	-- Convert horizontal and vertical countes to int.
	int_h_counter_reg <= CONV_INTEGER(h_counter_reg);
	int_v_counter_reg <= CONV_INTEGER(v_counter_reg);

	-- VGA Output MUX
	vga_r_out_mux <= vga_r_active;
    vga_g_out_mux <= vga_g_active;
    vga_b_out_mux <= vga_b_active;
	
	-- If vga draws when active = 0, vga = 0, else vga = 1.
    vga_r_mux <= (active & active & active & active) AND vga_r_out_mux;
    vga_g_mux <= (active & active & active & active) AND vga_g_out_mux;
    vga_b_mux <= (active & active & active & active) AND vga_b_out_mux;
    
    -- Register Outputs.
    PROCESS (pixel_clock)
    BEGIN
        IF (RISING_EDGE(pixel_clock)) THEN
            vga_hs_reg_delayed <= vga_hs_reg;
            vga_vs_reg_delayed <= vga_vs_reg;
            vga_r_reg <= vga_r_mux;
            vga_g_reg <= vga_g_mux;
            vga_b_reg <= vga_b_mux;      
        END IF;
    END PROCESS;

    -- Assign outputs.
    active_out <= active;
	int_h_counter_reg_out <= int_h_counter_reg;
	int_v_counter_reg_out <= int_v_counter_reg;
    vga_hs <= vga_hs_reg_delayed;
    vga_vs <= vga_vs_reg_delayed;
    vga_r <= vga_r_reg;
    vga_g <= vga_g_reg;
    vga_b <= vga_b_reg;

END behavioral;
