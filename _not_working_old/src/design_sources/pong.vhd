LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.MATH_REAL.ALL;

ENTITY pong IS
    PORT
    (
        pixel_clock : IN STD_LOGIC;
        btnu : IN STD_LOGIC; -- Up Player 1
        btnl : IN STD_LOGIC; -- Down Player 1
        btnr : IN STD_LOGIC; -- Up Player 2
        btnd : IN STD_LOGIC; -- Down Player 2
		active : IN STD_LOGIC;
		int_h_counter_reg : IN INTEGER;
		int_v_counter_reg : IN INTEGER;
		vga_r_active : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		vga_g_active : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		vga_b_active : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END pong;

ARCHITECTURE behavioral OF pong IS

-- 60Hz Clock for  Animation.
SIGNAL int_animation_clock : INTEGER RANGE 0 to 1800000 := 0;
SIGNAL int_animation_tick : INTEGER RANGE 0 to 1 := 0;

-- Player 1 Config.
SIGNAL int_player1_height : INTEGER := 64;
SIGNAL int_player1_width : INTEGER := 8;
SIGNAL int_player1_start_h : INTEGER := 44;
SIGNAL int_player1_start_v : INTEGER := 512;
SIGNAL int_player1_position_h : INTEGER := int_player1_start_h;
SIGNAL int_player1_position_v : INTEGER := int_player1_start_v;
SIGNAL int_player1_velocity_v : INTEGER RANGE -10 TO 10 := 1;

-- Player 2 Config.
SIGNAL int_player2_height : INTEGER := 64;
SIGNAL int_player2_width : INTEGER := 8;
SIGNAL int_player2_start_h : INTEGER := 1236;
SIGNAL int_player2_start_v : INTEGER := 512;
SIGNAL int_player2_position_h : INTEGER := int_player2_start_h;
SIGNAL int_player2_position_v : INTEGER := int_player2_start_v;
SIGNAL int_player2_velocity_v : INTEGER RANGE -10 TO 10 := 1;

-- Ball Config.
SIGNAL int_ball_height : INTEGER := 8;
SIGNAL int_ball_width : INTEGER := 8;
SIGNAL int_ball_start_h : INTEGER := 645;
SIGNAL int_ball_start_v : INTEGER := 512;
SIGNAL int_ball_position_h : INTEGER := int_ball_start_h;
SIGNAL int_ball_position_v : INTEGER := int_ball_start_v;
SIGNAL int_ball_velocity_h : INTEGER RANGE -10 TO 10 := 1;
SIGNAL int_ball_velocity_v : INTEGER RANGE -10 TO 10 := 1;

BEGIN
    PROCESS (pixel_clock)
    BEGIN
        IF(RISING_EDGE(pixel_clock)) THEN
            IF
            (
                int_animation_clock = 1800000
            ) THEN
                int_animation_tick <= 1;
            ELSE
                int_animation_tick <= 0;
            END IF;
            int_animation_clock <= int_animation_clock +1;
        END IF;
    END PROCESS;

	PROCESS (pixel_clock,btnu,btnl,btnr,btnd)
	BEGIN
		IF (RISING_EDGE(pixel_clock)) THEN

			-- Right Wall.
			IF (int_ball_position_h > 1280) THEN
		    	int_player1_position_v <= 512;
		      	int_ball_position_h <= 1280;
		      	int_ball_velocity_h <= 0 - int_ball_velocity_h;

		    -- Left Wall.
			ELSIF (int_ball_position_h < 4) THEN
		      	int_ball_position_h <= 4;
		      	int_ball_velocity_h <= 4 - int_ball_velocity_h;

		    -- Top Wall.
			ELSIF (int_ball_position_v < 0) THEN
		      	int_ball_position_v <= 0;
		      	int_ball_velocity_v <= 0 - int_ball_velocity_v;

		    -- Bottom Wall.
			ELSIF (int_ball_position_v > 1020) THEN
		    	int_player1_position_v <= 512;
		      	int_ball_position_v <= 1020;
		      	int_ball_velocity_v <= 0 - int_ball_velocity_v;
		    
		    -- Animate.
		    ELSIF (int_animation_tick = 1) THEN
		      	int_ball_position_h <= int_ball_position_h + int_ball_velocity_h;
		      	int_ball_position_v <= int_ball_position_v + int_ball_velocity_v;
		      	int_player2_position_v <= int_player2_position_v + int_player2_velocity_v;
				  
				-- Move Player 1 Up.
				IF (btnu = '1') THEN
		      		int_player1_position_v <= int_player1_position_v - int_player1_velocity_v;

				-- Move Player 1 Down.
				ELSIF (btnl = '1') THEN
		      		int_player1_position_v <= int_player1_position_v + int_player1_velocity_v;
				END IF;

				-- Move Player 2 Up.
				IF (btnr = '1') THEN
		      		int_player2_position_v <= int_player2_position_v - int_player2_velocity_v;

				-- Move Player 2 Down.
				ELSIF (btnd = '1') THEN
		      		int_player2_position_v <= int_player2_position_v + int_player2_velocity_v;
				END IF;
			
			-- Render Player 1.
			ELSIF
			(
				(active = '1') AND
				(int_h_counter_reg >= int_player1_position_h) AND (int_h_counter_reg <= int_player1_position_h + int_player1_width) AND
				(int_v_counter_reg >= int_player1_position_v - int_player1_height / 2) AND (int_v_counter_reg <= int_player1_position_v + int_player1_height / 2)
			) THEN
				vga_r_active <= "0011";
				vga_g_active <= "0011";
				vga_b_active <= "0011";

			-- Render Player 2.
			ELSIF
			(
				(active = '1') AND
				(int_h_counter_reg >= int_player2_position_h - int_player2_width) AND (int_h_counter_reg <= int_player2_position_h) AND
				(int_v_counter_reg >= int_player2_position_v - int_player2_height / 2) AND (int_v_counter_reg <= int_player2_position_v + int_player2_height / 2)
			) THEN
				vga_r_active <= "0011";
				vga_g_active <= "0011";
				vga_b_active <= "0011";

			-- Render Ball.
			ELSIF
			(
				(active = '1') AND
				(int_h_counter_reg >= int_ball_position_h - int_ball_width) AND (int_h_counter_reg <= int_ball_position_h) AND
				(int_v_counter_reg >= int_ball_position_v - int_ball_height / 2) AND (int_v_counter_reg <= int_ball_position_v + int_ball_height / 2)
			) THEN
				vga_r_active <= "0011";
				vga_g_active <= "0011";
				vga_b_active <= "0011";

			-- Render Nothing In-between.
			ELSE
				vga_r_active <= "0000";
				vga_g_active <= "0000";
				vga_b_active <= "0000";
			END IF;
			
		END IF;
	END PROCESS;
	
END behavioral;
