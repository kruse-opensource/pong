LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.MATH_REAL.ALL;

ENTITY testbench IS
END testbench;

ARCHITECTURE structure OF testbench IS

COMPONENT vga_controller
	PORT
	(
		clk100mhz : IN STD_LOGIC;
        btnu : IN STD_LOGIC; -- Up Player 1
        btnl : IN STD_LOGIC; -- Down Player 1
        btnr : IN STD_LOGIC; -- Up Player 2
        btnd : IN STD_LOGIC; -- Down Player 2
        vga_hs : OUT STD_LOGIC;
        vga_vs : OUT STD_LOGIC;
        vga_r : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_g : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        vga_b : OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
        sw : IN STD_LOGIC_VECTOR(11 DOWNTO 0)
	);
END COMPONENT;

COMPONENT pong
	PORT
	(
		pixel_clock : IN STD_LOGIC;
        btnu : IN STD_LOGIC; -- Up Player 1
        btnl : IN STD_LOGIC; -- Down Player 1
        btnr : IN STD_LOGIC; -- Up Player 2
        btnd : IN STD_LOGIC; -- Down Player 2
		active : IN STD_LOGIC;
		int_h_cntr_reg : IN integer;
		int_v_cntr_reg : IN integer;
		bg_red : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_green : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_blue : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_red_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_green_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		bg_blue_dly : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

SIGNAL clk100mhz : STD_LOGIC;
SIGNAL btnu : STD_LOGIC; -- Up Player 1
SIGNAL btnl : STD_LOGIC; -- Down Player 1
SIGNAL btnr : STD_LOGIC; -- Up Player 2
SIGNAL btnd : STD_LOGIC; -- Down Player 2
SIGNAL vga_hs : STD_LOGIC;
SIGNAL vga_vs : STD_LOGIC;
SIGNAL vga_r : STD_LOGIC_VECTOR (3 DOWNTO 0);
SIGNAL vga_g : STD_LOGIC_VECTOR (3 DOWNTO 0);
SIGNAL vga_b : STD_LOGIC_VECTOR (3 DOWNTO 0);
SIGNAL sw : STD_LOGIC_VECTOR(11 DOWNTO 0);

SIGNAL active : STD_LOGIC;
SIGNAL int_h_cntr_reg : integer;
SIGNAL int_v_cntr_reg : integer;
SIGNAL bg_red : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL bg_green : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL bg_blue : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL bg_red_dly : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL bg_green_dly : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL bg_blue_dly : STD_LOGIC_VECTOR(3 DOWNTO 0);

-- 108 MHz Clock
CONSTANT clk_period : TIME := 9.259259259259259 ns;
SIGNAL clk108mhz : STD_LOGIC;

BEGIN

	pong_inst : pong
		PORT MAP
		(
			pixel_clock => clk108mhz,
            btnu => btnu,
            btnl => btnl,
            btnr => btnr,
            btnd => btnd,
			active => active,
			int_h_cntr_reg => int_h_cntr_reg,
			int_v_cntr_reg => int_v_cntr_reg,
			bg_red => bg_red,
			bg_green => bg_green,
			bg_blue => bg_blue,
			bg_red_dly => bg_red_dly,
			bg_green_dly => bg_green_dly,
			bg_blue_dly => bg_blue_dly
		);

	vga_controller_inst : vga_controller
		PORT MAP
		(
			clk100mhz => clk108mhz,
            btnu => btnu,
            btnl => btnl,
            btnr => btnr,
            btnd => btnd,
            vga_hs => vga_hs,
            vga_vs => vga_vs,
            vga_r => vga_r,
            vga_g => vga_g,
            vga_b => vga_b,
            sw => sw
		);

    clk : process
    begin
        clk108mhz <= '0';
        wait for clk_period/2;
        clk108mhz <= '1';
        wait for clk_period/2;
    end process;
    
    --clk100mhz <= clk108mhz;

END structure;
